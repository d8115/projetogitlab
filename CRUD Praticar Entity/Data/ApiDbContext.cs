﻿using CRUD_Praticar_Entity.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace CRUD_Praticar_Entity.Data
{
    public class ApiDbContext : DbContext
    {
        public DbSet<Produto> Produtos { get; set; }
        public DbSet<Marca> Marcas { get; set; }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Role> Roles    { get; set; }
        public DbSet<Compra> Compras { get; set; }

        public ApiDbContext(DbContextOptions<ApiDbContext> opt) : base(opt)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            
            modelBuilder.Entity<Produto>().HasKey(k => k.Id);
            modelBuilder.Entity<Produto>().HasOne(m => m.Marca).WithMany(mm => mm.Produto).HasForeignKey(mmm => mmm.MarcaId);

            modelBuilder.Entity<Marca>().HasKey(k => k.Id);

            modelBuilder.Entity<Usuario>().HasKey(k => k.Id);
            modelBuilder.Entity<Usuario>().HasOne(m => m.Role).WithMany(mm => mm.Usuario).HasForeignKey(m => m.RoleId);
            modelBuilder.Entity<Usuario>().HasMany(c => c.Compras).WithOne(cc => cc.Usuario).HasForeignKey(ccc => ccc.Id);

            modelBuilder.Entity<Role>().HasKey(k => k.Id);

            modelBuilder.Entity<Compra>().HasKey(k => k.Id);

            DefaultUsers.CriaTagParaUsuariosNormais(modelBuilder);
            DefaultUsers.CriaUsuarioAdmin(modelBuilder);
        }
    }
}
