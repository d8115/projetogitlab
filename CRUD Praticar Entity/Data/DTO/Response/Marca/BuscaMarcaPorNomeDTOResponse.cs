﻿using CRUD_Praticar_Entity.Models;
using System.Collections.Generic;

namespace CRUD_Praticar_Entity.Data.DTO.Response.Marca
{
    public class BuscaMarcaPorNomeDTOResponse
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public List<string> ProdutosComAMarca { get; set; }
        public bool IsSuccess { get; set; }
    }
}
