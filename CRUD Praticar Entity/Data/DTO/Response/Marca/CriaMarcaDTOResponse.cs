﻿using System;

namespace CRUD_Praticar_Entity.Data.DTO.Response.Marca
{
    public class CriaMarcaDTOResponse
    {
        public string Message   { get; set; }
        public DateTime DataCriacao { get; set; }
    }
}
