﻿using System;

namespace CRUD_Praticar_Entity.Data.DTO.Response.Marca
{
    public class DeletaMarcaDTOResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
        public DateTime DataDelete { get; set; }
    }
}
