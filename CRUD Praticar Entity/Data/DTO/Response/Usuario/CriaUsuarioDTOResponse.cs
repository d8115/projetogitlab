﻿using System;

namespace CRUD_Praticar_Entity.Data.DTO.Response
{
    public class CriaUsuarioDTOResponse
    {
        public string Message { get; set; }
        public DateTime DataCriacao { get; set; }
    }
}
