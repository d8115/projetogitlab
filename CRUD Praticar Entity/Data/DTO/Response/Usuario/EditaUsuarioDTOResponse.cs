﻿using System;

namespace CRUD_Praticar_Entity.Data.DTO.Response
{
    public class EditaUsuarioDTOResponse
    {
        public string Message { get; set; }
        public DateTime DataEdicao { get; set; }
        public bool IsSuccess { get; set; }
    }
}
