﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Usuario
{
    public class BuscaUsuarioPorNomeDTOResponse
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int RoleId { get; set; }

        public string RoleName { get; set; }

        public bool Exist { get; set; }
    }
}
