﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Usuario
{
    public class LoginDTOResponse
    {
        public string Token { get; set; }
        public bool IsSuccess { get; set; }
    }
}
