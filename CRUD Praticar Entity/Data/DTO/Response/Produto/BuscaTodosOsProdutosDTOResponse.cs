﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Produto
{
    public class BuscaTodosOsProdutosDTOResponse
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public MarcaTodosOsProdutos Marca { get; set; }
    }

    public class MarcaTodosOsProdutos
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
