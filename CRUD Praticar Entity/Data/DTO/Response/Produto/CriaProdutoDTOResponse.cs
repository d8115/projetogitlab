﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Produto
{
    public class CriaProdutoDTOResponse
    {
        public string Message { get; set; }
        public bool IsSuccess { get; set; }
    }
}
