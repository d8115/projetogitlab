﻿using System;

namespace CRUD_Praticar_Entity.Data.DTO.Response.Produto
{
    public class DeletaProdutoDTOResponse
    {
        public string Message { get; set; }
        public DateTime DataDeDelete { get; set; }
    }
}
