﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Produto
{
    public class BuscaProdutoPorNomeDTOResponse
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public string MarcaNome { get; set; }
        public bool IsSuccess { get; set; }
    }
}
