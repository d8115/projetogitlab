﻿namespace CRUD_Praticar_Entity.Data.DTO.Response.Produto
{
    public class EditaProdutoDTOResponse
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }
}
