﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRUD_Praticar_Entity.Data.DTO.Request
{
    public class CriaUsuarioDTORequest
    {
        public const int MinLenght = 6;
        public const int MaxLenght = 20;

        [Required(ErrorMessage = "Digite um Nome de Usuario")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Digite uma Senha")]
        [StringLength(MaxLenght,  MinimumLength = MinLenght, ErrorMessage = "Digite uma Senha com no Minimo = 6 / Maximo = 20 caracteres")]
        public string Password { get; set; }
    }
}
