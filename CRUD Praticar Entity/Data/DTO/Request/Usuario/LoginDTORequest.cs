﻿namespace CRUD_Praticar_Entity.Data.DTO.Request.Usuario
{
    public class LoginDTORequest
    {
        public string Nome { get; set; }
        public string Password { get; set; }
    }
}
