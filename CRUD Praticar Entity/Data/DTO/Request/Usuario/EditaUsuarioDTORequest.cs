﻿using System.ComponentModel.DataAnnotations;

namespace CRUD_Praticar_Entity.Data.DTO.Request
{
    public class EditaUsuarioDTORequest
    {
        [Required(ErrorMessage = "Digite a role que ira mudar do usuario")]
        public string Role { get; set; }
    }
}
