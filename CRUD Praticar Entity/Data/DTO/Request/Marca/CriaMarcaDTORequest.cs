﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRUD_Praticar_Entity.Data.DTO.Request.Marca
{
    public class CriaMarcaDTORequest
    {
        private const int MinMarcaLenght = 2;
        private const int MaxMarcaLenght = 25;
        private const string MSG = "O Nome precisa ter 2 - 25 caracteres";

        [Required(ErrorMessage = "Insira o nome da Marca")]
        [StringLength(MaxMarcaLenght, MinimumLength = MinMarcaLenght, ErrorMessage = MSG)]
        public string Nome { get; set; }
    }
}
