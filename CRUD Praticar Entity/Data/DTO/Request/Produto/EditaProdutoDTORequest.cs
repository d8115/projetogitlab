﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRUD_Praticar_Entity.Data.DTO.Request.Produto
{
    public class EditaProdutoDTORequest
    {
        [Required(ErrorMessage = "Digite um Preco")]
        [Range(1, 999999999999999, ErrorMessage = "Digite um Preco de 1 - 999.999.999.999.999")]
        public decimal Preco { get; set; }
        [Required(ErrorMessage = "Digite o Id da Marca")]
        public int MarcaId { get; set; }
    }
}
