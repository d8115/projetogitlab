﻿using CRUD_Praticar_Entity.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace CRUD_Praticar_Entity.Data
{
    public class DefaultUsers
    {
        public static void CriaUsuarioAdmin(ModelBuilder modelBuilder)
        {
            int defaultId = 9999;

            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Id = defaultId,
                Name = "Admin",
            });

            modelBuilder.Entity<Usuario>().HasData(new Usuario()
            {
                Id = 9999,
                Username = "Administrador",
                RoleId = defaultId,
                Password = "1234",
            });
        }

        public static void CriaTagParaUsuariosNormais(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Role>().HasData(new Role()
            {
                Name = "Padrao",
                Id = 9998
            });
        }
    }
}
