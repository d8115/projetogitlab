﻿using System.Collections.Generic;

namespace CRUD_Praticar_Entity.Models
{
    public class Marca
    {
        public int Id { get; set; }
        public string Nome { get; set; }

        public virtual List<Produto> Produto { get; set; }
    }
}
