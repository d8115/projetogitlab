﻿using System.Collections.Generic;

namespace CRUD_Praticar_Entity.Models
{
    public class Usuario
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public int RoleId { get; set; }
        public string Password { get; set; }

        public virtual List<Compra> Compras { get; set; }
        public virtual Role Role { get; set; }
    }
}
