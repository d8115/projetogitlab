﻿using System.Collections.Generic;

namespace CRUD_Praticar_Entity.Models
{
    public class Compra
    {
        public int Id { get; set; } 
        public int UsuarioId { get; set; }
        public string NomeUsuario { get; set; }
        public int ProdutoId { get; set; }
        public string NomeProduto { get; set; }
        public int QuantidadeProduto { get; set; }

        public virtual Produto Produto { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
