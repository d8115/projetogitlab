﻿using System.Collections.Generic;

namespace CRUD_Praticar_Entity.Models
{
    public class Role
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual List<Usuario> Usuario { get; set; }
    }
}
