﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace CRUD_Praticar_Entity.Migrations
{
    public partial class AdicionandoRoleparaUsuariosNormais : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Roles",
                columns: new[] { "Id", "Name" },
                values: new object[] { 9998, "Padrao" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Roles",
                keyColumn: "Id",
                keyValue: 9998);
        }
    }
}
