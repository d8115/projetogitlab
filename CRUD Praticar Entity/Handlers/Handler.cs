﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Models;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers
{
    public abstract class Handler : HandlerBase
    {
        public Handler(ApiDbContext context) : base(context)
        {
        }

        protected List<Usuario> BuscaTodosUsuarios()
        {
            return context.Usuarios.Include(u => u.Role).ToList();
        }

        protected List<Role> BuscaTodasRoles()
        {
            return context.Roles.ToList();
        }

        protected List<Produto> BuscaTodosProdutos()
        {
            return context.Produtos.ToList();
        }

        protected List<Marca> BucasTodasMarcas()
        {
            return context.Marcas.ToList();
        }

        protected Usuario BuscaUsuarioPorNome(string Username)
        {
            return context.Usuarios
                .Where(n => n.Username == Username)
                .FirstOrDefault();
        }

        protected Role BuscaRolePorNome(string Nome)
        {
            return context.Roles
                .Where(n => n.Name == Nome)
                .FirstOrDefault();
        }

        protected Marca BuscaMarcaPorNome(string Nome)
        {
            return context.Marcas
                .Where(n => n.Nome == Nome)
                .FirstOrDefault();
        }

        protected Produto BuscaProdutoPorNome(string Nome)
        {
            return context.Produtos
                .Where(n => n.Nome == Nome)
                .FirstOrDefault();
        }
    }
}
