﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request;
using CRUD_Praticar_Entity.Data.DTO.Response;
using CRUD_Praticar_Entity.Models;
using System;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class CriaUsuarioHandler : Handler
    {
        public CriaUsuarioHandler(ApiDbContext context) : base(context)
        {
        }

        public CriaUsuarioDTOResponse Handler(CriaUsuarioDTORequest usuarioDTORequest)
        {
            context.Add(new Usuario()
            {
                Username = usuarioDTORequest.Username,
                RoleId = 9998,
                Password = usuarioDTORequest.Password,
            });

            context.SaveChanges();

            CriaUsuarioDTOResponse response = new CriaUsuarioDTOResponse()
            {
                Message = "Usuario Criado Com Sucesso",
                DataCriacao = DateTime.Now,
            };

            return response;
        }
    }
}
