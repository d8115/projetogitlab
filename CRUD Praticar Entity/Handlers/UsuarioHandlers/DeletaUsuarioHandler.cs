﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Usuario;
using CRUD_Praticar_Entity.Models;
using System;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class DeletaUsuarioHandler : Handler
    {
        public DeletaUsuarioHandler(ApiDbContext context) : base(context)
        {
        }

        public DeletaUsuarioDTOResponse Handler(string nomerequest, string Nome, string role)
        {
            if (nomerequest != Nome && role != "Admin")
                return new DeletaUsuarioDTOResponse()
                {
                    Message = "Você não possui permissão para isso",
                    DataDelete = DateTime.Now,
                    IsSuccess = false,
                };

            Usuario usuario = BuscaUsuarioPorNome(Nome);
            if (usuario == null)
            {
                return new DeletaUsuarioDTOResponse()
                {
                    Message = "Usuario Inexistente",
                    DataDelete = DateTime.Now,
                    IsSuccess = false
                };
            }

            context.Remove(usuario);
            context.SaveChanges();

            return new DeletaUsuarioDTOResponse()
            {
                Message = "Usuario Deletado Com Sucesso",
                IsSuccess = true,
                DataDelete = DateTime.Now,
            };
        }
    }
}
