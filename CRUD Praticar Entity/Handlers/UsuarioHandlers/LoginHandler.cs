﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Usuario;
using CRUD_Praticar_Entity.Data.DTO.Response.Usuario;
using CRUD_Praticar_Entity.Services;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class LoginHandler : Handler
    {
        public LoginHandler(ApiDbContext context) : base(context)
        {
        }

        public LoginDTOResponse Handler(LoginDTORequest loginDTO)
        {
            var user = BuscaUsuarioPorNome(loginDTO.Nome);
            var role = (from r in context.Roles
                       where r.Id == user.RoleId
                       select r.Name).FirstOrDefault();

            if (user == null)
                return new LoginDTOResponse()
                {
                    Token = "",
                    IsSuccess = false
                };

            var token = TokenService.GenerateToken(user.Username, role);
            return new LoginDTOResponse()
            {
                Token = token,
                IsSuccess = true    
            };
        }
    }
}
