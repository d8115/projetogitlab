﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Usuario;
using CRUD_Praticar_Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class BuscaTodosOsUsuariosHandler : Handler
    {
        public BuscaTodosOsUsuariosHandler(ApiDbContext context) : base(context)
        {
        }

        public List<BuscaTodosOsUsuarioDTOResponse> Handler()
        {
            var a = from u in context.Usuarios
                    join r in context.Roles on u.RoleId equals r.Id select new BuscaTodosOsUsuarioDTOResponse()
                    {
                        Id = u.Id,
                        Username = u.Username,
                        RoleId = u.RoleId,
                        RoleName = r.Name
                    };

            return a.ToList();
        }
    }
}
