﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request;
using CRUD_Praticar_Entity.Data.DTO.Response;
using CRUD_Praticar_Entity.Models;
using System;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class EditaUsuarioHandler : Handler
    {
        public EditaUsuarioHandler(ApiDbContext context) : base(context) { }
        public EditaUsuarioDTOResponse Handler(string nomerequest, string Nome, EditaUsuarioDTORequest editaUsuarioDTO, string roleSended)
        {
            if (nomerequest != Nome && roleSended != "Admin")
                return new EditaUsuarioDTOResponse()
                {
                    Message = "Você não possui permissão para isso",
                    IsSuccess = false,
                };

            Usuario usuario = BuscaUsuarioPorNome(Nome);

            var role = BuscaRolePorNome(editaUsuarioDTO.Role);
            
            if (role == null)
            {
                return new EditaUsuarioDTOResponse()
                {
                    Message = "Role Inexistente",
                    DataEdicao = DateTime.Now,
                };
            }

            if (usuario == null)
                return new EditaUsuarioDTOResponse()
                {
                    Message = "Usuario Inexistente",
                    DataEdicao = DateTime.Now,
                };

            context.Usuarios.Where(n => n.Id == usuario.Id).FirstOrDefault().RoleId = role.Id;
            context.SaveChanges();

            return new EditaUsuarioDTOResponse()
            {
                Message = "Usuario Modificado com Sucesso",
                DataEdicao = DateTime.Now,
            };
        }
    }
}
