﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Usuario;
using CRUD_Praticar_Entity.Models;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.UsuarioHandlers
{
    public class BuscaUsuarioPorNomeHandler : Handler
    {
        public BuscaUsuarioPorNomeHandler(ApiDbContext context) : base(context)
        {
        }

        public BuscaUsuarioPorNomeDTOResponse Handler(string Nome)
        {
            var usuario =   from u in context.Usuarios
                            where u.Username == Nome
                            join r in context.Roles on u.RoleId equals r.Id
                            select new BuscaUsuarioPorNomeDTOResponse()
                            {
                                 Id = u.Id,
                                 Username = u.Username,
                                 RoleId = r.Id,
                                 RoleName = r.Name,
                                 Exist = true
                            };

            if (usuario.FirstOrDefault() == null)
            {
                return new BuscaUsuarioPorNomeDTOResponse(){
                    Exist = false,
                };
            }

            return usuario.FirstOrDefault();
        }
    }
}
