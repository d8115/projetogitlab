﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Marca;
using CRUD_Praticar_Entity.Data.DTO.Response.Marca;
using CRUD_Praticar_Entity.Models;
using System;

namespace CRUD_Praticar_Entity.Handlers.MarcaHandlers
{
    public class CriaMarcaHandler : Handler
    {
        public CriaMarcaHandler(ApiDbContext context) : base(context)
        {
        }

        public CriaMarcaDTOResponse Handler(CriaMarcaDTORequest marcaDTO)
        {
            Marca marca = new Marca()
            {
                Nome = marcaDTO.Nome,
            };

            context.Add(marca);
            context.SaveChanges();

            return new CriaMarcaDTOResponse()
            {
                Message = "Marca Criada com Sucesso",
                DataCriacao = DateTime.Now
            };
        }
    }
}
