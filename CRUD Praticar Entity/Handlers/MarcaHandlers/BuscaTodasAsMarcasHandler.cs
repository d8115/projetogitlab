﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Marca;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.MarcaHandlers
{
    public class BuscaTodasAsMarcasHandler : Handler
    {
        public BuscaTodasAsMarcasHandler(ApiDbContext context) : base(context)
        {
        }

        public List<BuscaMarcaDTOResponse> Handler()
        {
            return context.Marcas.Select(m => new BuscaMarcaDTOResponse() { Nome = m.Nome, Id = m.Id}).ToList();
        }
    }
}
