﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Marca;
using CRUD_Praticar_Entity.Models;
using System;

namespace CRUD_Praticar_Entity.Handlers.MarcaHandlers
{
    public class DeletaMarcaHandler : Handler
    {
        public DeletaMarcaHandler(ApiDbContext context) : base(context)
        {
        }

        public DeletaMarcaDTOResponse Handler(string Nome)
        {
            Marca marca = BuscaMarcaPorNome(Nome);
            if (marca == null)
            {
                return new DeletaMarcaDTOResponse()
                {
                    Message = "Marca Inexistente",
                    DataDelete = DateTime.Now,
                    IsSuccess = false
                };
            }

            context.Remove(marca);
            context.SaveChanges();

            return new DeletaMarcaDTOResponse()
            {
                Message = "Marca Deletada Com Sucesso",
                IsSuccess = true,
                DataDelete = DateTime.Now,
            };
        }
    }
}
