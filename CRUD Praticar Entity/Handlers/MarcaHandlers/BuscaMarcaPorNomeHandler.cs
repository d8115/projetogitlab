﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Marca;
using CRUD_Praticar_Entity.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.MarcaHandlers
{
    public class BuscaMarcaPorNomeHandler : Handler
    {
        public BuscaMarcaPorNomeHandler(ApiDbContext context) : base(context)
        {
        }

        public BuscaMarcaPorNomeDTOResponse Handler(string nome)
        {
            Marca marca = BuscaMarcaPorNome(nome);

            BuscaMarcaPorNomeDTOResponse marcaDTO = new BuscaMarcaPorNomeDTOResponse()
            {
                Id = marca.Id,
                Nome = marca.Nome,
                ProdutosComAMarca = (from p in context.Produtos
                                    join m in context.Marcas
                                    on p.MarcaId equals m.Id
                                    where m.Nome == nome
                                    select p.Nome).ToList()
            };

            return marcaDTO;
        }
    }
}
