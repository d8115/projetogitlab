﻿using CRUD_Praticar_Entity.Data;

namespace CRUD_Praticar_Entity.Handlers
{
    public abstract class HandlerBase
    {
        public ApiDbContext context;
        public HandlerBase(ApiDbContext context)
        {
            this.context = context;
        }
    }
}
