﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Produto;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Models;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.ProdutoHandlers
{
    public class EditaProdutoHandler : Handler
    {
        public EditaProdutoHandler(ApiDbContext context) : base(context)
        {
        }

        public EditaProdutoDTOResponse Handler(EditaProdutoDTORequest produtoDTO, string nome)
        {
            Produto produto = BuscaProdutoPorNome(nome);

            if (produto == null)
                return new EditaProdutoDTOResponse()
                {
                    Message = "Produto Inexistente",
                    IsSuccess = false,
                };

            produto.MarcaId = produtoDTO.MarcaId;
            produto.Preco = produtoDTO.Preco;

            context.SaveChanges();

            return new EditaProdutoDTOResponse()
            {
                Message = "Produto Modificado com sucesso",
                IsSuccess = true,
            };
        }
    }
}
