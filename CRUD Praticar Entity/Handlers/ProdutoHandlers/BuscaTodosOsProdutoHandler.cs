﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Models;
using System.Collections.Generic;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.ProdutoHandlers
{
    public class BuscaTodosOsProdutoHandler : Handler
    {
        public BuscaTodosOsProdutoHandler(ApiDbContext context) : base(context)
        {
        }

        public List<BuscaTodosOsProdutosDTOResponse> Handler()
        {
            var a = (from p in context.Produtos
                    join m in context.Marcas
                    on p.Id equals m.Id
                    select new BuscaTodosOsProdutosDTOResponse()
                    {
                        Id = p.Id,
                        Nome = p.Nome,
                        Preco = p.Preco,
                        Marca = new MarcaTodosOsProdutos()
                        {
                            Nome = m.Nome,
                            Id = m.Id,
                        }
                    }).ToList();

            return a;
        }
    }
}
