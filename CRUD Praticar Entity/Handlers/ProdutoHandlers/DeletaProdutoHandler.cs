﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Produto;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Models;
using System;

namespace CRUD_Praticar_Entity.Handlers.ProdutoHandlers
{
    public class DeletaProdutoHandler : Handler
    {
        public DeletaProdutoHandler(ApiDbContext context) : base(context)
        {
        }

        public DeletaProdutoDTOResponse Handler(string nome)
        {
            Produto produto = BuscaProdutoPorNome(nome);

            context.Remove(produto);
            context.SaveChanges();

            return new DeletaProdutoDTOResponse()
            {
                Message = "Produto Deletado com Sucesso",
                DataDeDelete = DateTime.Now
            };
        }
    }
}
