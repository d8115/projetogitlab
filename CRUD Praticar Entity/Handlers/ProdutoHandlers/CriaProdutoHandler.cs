﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Produto;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Models;

namespace CRUD_Praticar_Entity.Handlers.ProdutoHandlers
{
    public class CriaProdutoHandler : Handler
    {
        public CriaProdutoHandler(ApiDbContext context) : base(context)
        {
        }

        public CriaProdutoDTOResponse Handler(CriaProdutoDTORequest produtoDTO)
        {
            Produto produto = new Produto()
            {
                Nome = produtoDTO.Nome,
                MarcaId = produtoDTO.MarcaId,
                Preco = produtoDTO.Preco,
            };

            context.Add(produto);
            context.SaveChanges();

            return new CriaProdutoDTOResponse()
            {
                Message = "Produto Criado com Sucesso",
                IsSuccess = true,
            };
        }
    }
}
