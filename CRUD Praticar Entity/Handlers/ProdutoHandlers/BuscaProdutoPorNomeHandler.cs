﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Models;
using System.Linq;

namespace CRUD_Praticar_Entity.Handlers.ProdutoHandlers
{
    public class BuscaProdutoPorNomeHandler : Handler
    {
        public BuscaProdutoPorNomeHandler(ApiDbContext context) : base(context)
        {
        }

        public BuscaProdutoPorNomeDTOResponse Handler(string nome)
        {
            Produto produto = BuscaProdutoPorNome(nome);

            Marca marca = context.Marcas.Where(m => m.Id == produto.MarcaId).FirstOrDefault();

            if (produto == null)
            {
                return new BuscaProdutoPorNomeDTOResponse()
                {
                    IsSuccess = false,
                };
            }

            return new BuscaProdutoPorNomeDTOResponse()
            {
                Id = produto.Id,
                Nome = produto.Nome,
                Preco = produto.Preco,
                IsSuccess = true,
                MarcaNome = marca.Nome
            };
        }
    }
}
