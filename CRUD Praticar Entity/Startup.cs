using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Handlers;
using CRUD_Praticar_Entity.Handlers.MarcaHandlers;
using CRUD_Praticar_Entity.Handlers.ProdutoHandlers;
using CRUD_Praticar_Entity.Handlers.UsuarioHandlers;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRUD_Praticar_Entity
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApiDbContext>(options => options.UseMySQL(Configuration.GetConnectionString("Default")));
            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "CRUD_Praticar_Entity", Version = "v1" });
                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "JWT Authorization header using the Bearer scheme (Example: 'Bearer 12345abcdef')",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Scheme = "Bearer"
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement
            {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = "Bearer"
                        }
                    },
                    Array.Empty<string>()
                }
            });
            });

            var key = Encoding.ASCII.GetBytes("mysupersecret_secretkey!123");

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                    options.SaveToken = true;
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(key),
                        ValidateIssuer = false,
                        ValidateAudience = false
                    };
                });

            services.AddScoped<CriaUsuarioHandler, CriaUsuarioHandler>();
            services.AddScoped<EditaUsuarioHandler, EditaUsuarioHandler>();
            services.AddScoped<DeletaUsuarioHandler, DeletaUsuarioHandler>();
            services.AddScoped<BuscaTodosOsUsuariosHandler, BuscaTodosOsUsuariosHandler>();
            services.AddScoped<BuscaUsuarioPorNomeHandler, BuscaUsuarioPorNomeHandler>();
            services.AddScoped<LoginHandler, LoginHandler>();

            services.AddScoped<BuscaTodasAsMarcasHandler, BuscaTodasAsMarcasHandler>();
            services.AddScoped<BuscaMarcaPorNomeHandler, BuscaMarcaPorNomeHandler>();
            services.AddScoped<DeletaMarcaHandler, DeletaMarcaHandler>();
            services.AddScoped<CriaMarcaHandler, CriaMarcaHandler>();

            services.AddScoped<CriaProdutoHandler, CriaProdutoHandler>();
            services.AddScoped<EditaProdutoHandler, EditaProdutoHandler>();
            services.AddScoped<DeletaProdutoHandler, DeletaProdutoHandler>();
            services.AddScoped<BuscaProdutoPorNomeHandler, BuscaProdutoPorNomeHandler>();
            services.AddScoped<BuscaTodosOsProdutoHandler, BuscaTodosOsProdutoHandler>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "CRUD_Praticar_Entity v1"));
            }

            app.UseAuthentication();

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
