﻿using CRUD_Praticar_Entity.Data.DTO.Request.Produto;
using CRUD_Praticar_Entity.Data.DTO.Response.Produto;
using CRUD_Praticar_Entity.Handlers.ProdutoHandlers;
using CRUD_Praticar_Entity.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace CRUD_Praticar_Entity.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProdutoController : Controller
    {
        [HttpPost("cria")]
        [Authorize]
        public IActionResult Cria([FromServices]CriaProdutoHandler handler, [FromBody]CriaProdutoDTORequest produtoDTO)
        {
            return Ok(handler.Handler(produtoDTO));
        } 

        [HttpPatch("edita/{nome}")]
        public IActionResult Edita([FromServices]EditaProdutoHandler handler, [FromBody]EditaProdutoDTORequest produtoDTO, string nome)
        {
            EditaProdutoDTOResponse response = handler.Handler(produtoDTO,nome);
            if (response.IsSuccess)
                return NotFound(response);
            return Ok(response);

        }

        [HttpDelete("deleta/{nome}")]
        public IActionResult Deleta([FromServices]DeletaProdutoHandler handler, string nome)
        {
            return Ok(handler.Handler(nome));
        }

        [HttpGet("{nome}")]
        public IActionResult BuscaPorNome([FromServices] BuscaProdutoPorNomeHandler handler, string nome)
        {
            BuscaProdutoPorNomeDTOResponse response = handler.Handler(nome);
            if (response.IsSuccess)
                return NotFound();
            return Ok(response);

        }

        [HttpGet("todos")]
        public IActionResult BuscaTodos([FromServices] BuscaTodosOsProdutoHandler handler)
        {
            return Ok(handler.Handler());
        }
    }
}
