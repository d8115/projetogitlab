﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request.Marca;
using CRUD_Praticar_Entity.Data.DTO.Response.Marca;
using CRUD_Praticar_Entity.Handlers.MarcaHandlers;
using Microsoft.AspNetCore.Mvc;

namespace CRUD_Praticar_Entity.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class MarcaController : ControllerBase
    {
        public ApiDbContext context;
        public MarcaController(ApiDbContext context)
        {
            this.context = context;
        }

        [HttpPost("cria")]
        public IActionResult CriaMarca([FromServices] CriaMarcaHandler handler, [FromBody] CriaMarcaDTORequest criaMarcaDTO)
        {
            return Ok(handler.Handler(criaMarcaDTO));
        }

        [HttpDelete("deleta/{nome}")]
        public IActionResult Deleta([FromServices] DeletaMarcaHandler handler, string nome)
        {
            DeletaMarcaDTOResponse response = handler.Handler(nome);
            if (response.IsSuccess)
                return NotFound(response);
            return Ok(response);
        }

        [HttpGet("todas")]
        public IActionResult BuscaTodas([FromServices] BuscaTodasAsMarcasHandler handler)
        {
            return Ok(handler.Handler());
        }

        [HttpGet("{nome}")]
        public IActionResult BuscaPorNome([FromServices] BuscaMarcaPorNomeHandler handler,string nome)
        {
            BuscaMarcaPorNomeDTOResponse response = handler.Handler(nome);
            if(response.IsSuccess)
                return NotFound("Usuario Inexistente");
            return Ok(handler.Handler(nome));
        }
    }
}
