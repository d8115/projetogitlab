﻿using CRUD_Praticar_Entity.Data;
using CRUD_Praticar_Entity.Data.DTO.Request;
using CRUD_Praticar_Entity.Data.DTO.Request.Usuario;
using CRUD_Praticar_Entity.Data.DTO.Response;
using CRUD_Praticar_Entity.Data.DTO.Response.Usuario;
using CRUD_Praticar_Entity.Handlers;
using CRUD_Praticar_Entity.Handlers.UsuarioHandlers;
using CRUD_Praticar_Entity.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;

namespace CRUD_Praticar_Entity.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UsuarioController : Controller
    {
        public ApiDbContext context;
        public UsuarioController(ApiDbContext context)
        {
            this.context = context;
        }

        [HttpPost("cria")]
        [AllowAnonymous]
        public IActionResult CriaUsuario([FromServices]CriaUsuarioHandler handler, [FromBody]CriaUsuarioDTORequest criaUsuarioDTO)
        {
            return Ok(handler.Handler(criaUsuarioDTO));
        }

        [HttpPatch("edita/{Nome}")]
        [Authorize]
        public IActionResult EditaUsuario([FromServices] EditaUsuarioHandler handler, [FromBody] EditaUsuarioDTORequest editaUsuarioDTO, string Nome)
        {
            var role = User.Claims.Where(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Select(c => c.Value).FirstOrDefault();

            EditaUsuarioDTOResponse response = handler.Handler(User.Identity.Name, Nome, editaUsuarioDTO, role);

            if (!response.IsSuccess)
                return NotFound(response);

            return Ok(response);
        }

        [HttpDelete("deleta/{Nome}")]
        [Authorize]
        public IActionResult DeletaUsuario([FromServices]DeletaUsuarioHandler handler,string Nome)
        {
            var role = User.Claims.Where(c => c.Type == "http://schemas.microsoft.com/ws/2008/06/identity/claims/role").Select(c => c.Value).FirstOrDefault();

            DeletaUsuarioDTOResponse response = handler.Handler(User.Identity.Name, Nome, role);

            if (response.IsSuccess)
                return Ok(response);
            return BadRequest(response);
        }

        [HttpGet("todos")]
        public IActionResult BuscaTodosOsUsuarios([FromServices]BuscaTodosOsUsuariosHandler handler)
        {
            return Ok(handler.Handler());
        }

        [HttpGet("{Nome}")]
        [Authorize]
        public IActionResult BuscaUsuarioPorNome([FromServices]BuscaUsuarioPorNomeHandler handler, string Nome)
        {
            BuscaUsuarioPorNomeDTOResponse response = handler.Handler(Nome);
            if(!response.Exist)
            {
                return NotFound();
            }
            return Ok(response);
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult<dynamic>> Login([FromServices]LoginHandler handler, [FromBody]LoginDTORequest loginDTO)
        {
            LoginDTOResponse response = handler.Handler(loginDTO);
            if(!response.IsSuccess)
                return Task.FromResult(Unauthorized());

            return Task.FromResult(Ok(response));

        } 

    }
}
